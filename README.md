# Fruit or beer

Une application qui choisi pour vous si vous devez manger un fruit (et lequel) ou boire une bière (et laquelle).
Des sections Fruitopedia et Beeropedia sont disponibles.

## API utilisées

- [fruityvice](https://www.fruityvice.com/)
- [punkapi](https://punkapi.com/)

## Fonctionnalités

- Onglet 1: Un appui sur le bouton permet de savoir au hasard si on doit boire une bière ou manger un fruit.
- Onglet 2: La liste des fruits. Le bouton permet d'acceder aux favoris. Si on clique sur un fruit on accède au détails. Dans le détails on peut ajouter le fruit au favoris.
- Onglet 3: On peut chercher les bières en fonction du taux d'alcool. Un clic sur une biere permet de voir les détails. Dans les détails on peut ajouter la bière au favoris. Le bouton permet de voir les favoris.




