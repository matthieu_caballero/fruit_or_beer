import 'package:flutter/material.dart';
import 'package:fruit_or_beer/services/beer_service.dart';
import 'package:fruit_or_beer/services/fruit_service.dart';
import 'package:fruit_or_beer/services/localstorage_service.dart';
import 'package:fruit_or_beer/services/service.dart';
import 'package:fruit_or_beer/stores/fruit_store.dart';
import 'package:fruit_or_beer/ui/screens/beer_details.dart';
import 'package:fruit_or_beer/ui/screens/beer_favorites.dart';
import 'package:fruit_or_beer/ui/screens/fruit_facts.dart';
import 'package:fruit_or_beer/ui/screens/fruit_favorites.dart';
import 'package:fruit_or_beer/ui/screens/home.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final FruitService fruitService = FruitService();
  final BeerService beerService = BeerService();
  final LocalStorageService localStorageService = LocalStorageService();
  final Service service =
      Service(fruitService, beerService, localStorageService);
  final FruitStore fruitStore = FruitStore(service);

  await fruitStore.loadFruits();

  runApp(MultiProvider(providers: [
    Provider<FruitStore>(create: (_) => fruitStore),
    Provider<Service>(create: (_) => service)
  ], child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Fruit or beer app',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const Home(),
      routes: {
        '/home': (context) => const Home(),
        '/fruitfacts': (context) => const FruitFacts(),
        '/fruitfavorites': (context) => const FruitFavorites(),
        '/beerdetails': (context) => const BeerDetails(),
        '/beerfavorites': (context) => const BeerFavorites(),
      },
    );
  }
}
