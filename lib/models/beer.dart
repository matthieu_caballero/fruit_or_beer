import 'dart:convert';

class Beer {
  late int id;
  late String name;
  late String tagline;
  late String firstBrewed;
  late String description;
  late num abv;
  late List<String> foodPairing;
  late String brewersTips;
  late Ingredients ingredients;

  Beer(this.id, this.name, this.tagline, this.firstBrewed, this.description,
      this.abv, this.brewersTips, this.foodPairing, this.ingredients);

  factory Beer.fromJson(Map<String, dynamic> json) {
    List<String> foodPairing = [];
    json['food_pairing'].forEach((e) {
      foodPairing.add(e.toString());
    });
    return Beer(
        json['id'] as int,
        json['name'] as String,
        json['tagline'] as String,
        json['first_brewed'] as String,
        json['description'] as String,
        json['abv'] as num,
        json['brewers_tips'] as String,
        foodPairing,
        Ingredients.fromJson(json['ingredients']));
  }
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'tagline': tagline,
      'first_brewed': firstBrewed,
      'description': description,
      'abv': abv,
      'brewers_tips': brewersTips,
      'food_pairing': foodPairing.map((e) => jsonEncode(e)).toList(),
      'ingredients': ingredients.toJson()
    };
  }
}

class Ingredients {
  late String yeast;
  late List<Malt> malt;
  late List<Hop> hop;

  Ingredients(this.yeast, this.malt, this.hop);

  factory Ingredients.fromJson(Map<String, dynamic> json) {
    List<Malt> lstMalt = [];
    List<Hop> lstHop = [];
    json['malt'].forEach((malt) {
      lstMalt.add(Malt.fromJson(malt));
    });
    json['hops'].forEach((hop) {
      lstHop.add(Hop.fromJson(hop));
    });
    return Ingredients(json['yeast'] as String, lstMalt, lstHop);
  }

  Map<String, dynamic> toJson() {
    return {
      'yeast': yeast,
      'malt': malt.map((e) => e.toJson()).toList(),
      'hops': hop.map((e) => e.toJson()).toList()
    };
  }
}

class Malt {
  late String name;
  late Amount amount;

  Malt(this.name, this.amount);

  factory Malt.fromJson(Map<String, dynamic> json) {
    return Malt(json['name'] as String, Amount.fromJson(json['amount']));
  }

  Map<String, dynamic> toJson() {
    return {'name': name, 'amount': amount.toJson()};
  }
}

class Hop {
  late String name;
  late Amount amount;
  late String add;
  late String attribute;

  Hop(this.name, this.amount, this.add, this.attribute);

  factory Hop.fromJson(Map<String, dynamic> json) {
    return Hop(json['name'] as String, Amount.fromJson(json['amount']),
        json['add'] as String, json['attribute'] as String);
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'amount': amount.toJson(),
      'add': add,
      'attribute': attribute
    };
  }
}

class Amount {
  late String unit;
  late num value;

  Amount(this.unit, this.value);

  factory Amount.fromJson(Map<String, dynamic> json) {
    return Amount(json['unit'] as String, json['value'] as num);
  }

  Map<String, dynamic> toJson() {
    return {'unit': unit, 'value': value};
  }
}
