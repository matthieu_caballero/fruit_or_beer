import 'dart:convert';

/// Un fruit avec ses caractéristiques, notamment nutritionnelles
class Fruit {
  late int id;
  late String name;
  late String genus;
  late String family;
  late String order;
  late Nutrition nutrition;

  Fruit(
      this.id, this.name, this.genus, this.family, this.order, this.nutrition);

  String toJson() {
    return jsonEncode({
      'id': id,
      'name': name,
      'genus': genus,
      'family': family,
      'order': order,
      'nutritions': nutrition.toJson()
    });
  }

  factory Fruit.fromJson(Map<String, dynamic> json) {
    return Fruit(
        json['id'] as int,
        json['name'] as String,
        json['genus'] as String,
        json['family'] as String,
        json['order'] as String,
        Nutrition.fromJson(json['nutritions']));
  }
}

/// Les valeurs nutritionnelles d'un fruit
class Nutrition {
  late num carbohydrates;
  late num protein;
  late num fat;
  late num calories;
  late num sugar;

  Nutrition(
      this.carbohydrates, this.protein, this.fat, this.calories, this.sugar);

  factory Nutrition.fromJson(dynamic json) {
    return Nutrition(json['carbohydrates'] as num, json['protein'] as num,
        json['fat'] as num, json['calories'] as num, json['sugar'] as num);
  }

  String toJson() {
    return jsonEncode({
      'carbohydrates': carbohydrates,
      'protein': protein,
      'fat': fat,
      'calories': calories,
      'sugar': sugar
    });
  }
}
