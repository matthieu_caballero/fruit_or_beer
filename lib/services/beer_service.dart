import 'dart:convert';
import 'package:fruit_or_beer/models/beer.dart';
import 'package:http/http.dart';

class BeerService {
  Future<Beer> fetchRandomBeer() async {
    Response response =
        await get(Uri.parse('https://api.punkapi.com/v2/beers/random'));
    if (response.statusCode == 200) {
      dynamic json = jsonDecode(response.body);
      return Beer.fromJson(json[0]);
    } else {
      throw Exception('Failed to load addresses');
    }
  }

  Future<List<Beer>> fetchAbvBeer(String abvGt, String abvLt) async {
    Response response = await get(Uri.parse(
        'https://api.punkapi.com/v2/beers?abv_gt=$abvGt&abv_lt=$abvLt'));
    if (response.statusCode == 200) {
      List<dynamic> json = jsonDecode(response.body);
      return json.map((e) => Beer.fromJson(e)).toList();
    } else {
      throw Exception('Failed to load addresses');
    }
  }
}
