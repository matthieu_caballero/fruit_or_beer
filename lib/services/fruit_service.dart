import 'dart:convert';

import 'package:fruit_or_beer/models/fruit.dart';
import 'package:http/http.dart';

class FruitService {
  Future<List<Fruit>> fetchAllFruits() async {
    Response response =
        await get(Uri.parse('https://www.fruityvice.com/api/fruit/all'));
    if (response.statusCode == 200) {
      List<dynamic> json = jsonDecode(response.body);
      return json.map((e) => Fruit.fromJson(e)).toList();
    } else {
      throw Exception('Failed to load addresses');
    }
  }

  Future<Fruit> fetchFruitWithId(int fruitId) async {
    Response response =
        await get(Uri.parse('https://www.fruityvice.com/api/fruit/$fruitId'));
    if (response.statusCode == 200) {
      dynamic json = jsonDecode(response.body);
      return Fruit.fromJson(json);
    } else {
      throw Exception('Failed to load addresses');
    }
  }
}
