import 'dart:convert';

import 'package:fruit_or_beer/models/beer.dart';
import 'package:fruit_or_beer/models/fruit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalStorageService {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

// FRUITS -------------------------------------------------------------------------------------------
  Future<void> saveFavoriteFruits(List<Fruit> lstFruits) async {
    final SharedPreferences prefs = await _prefs;
    prefs.setStringList(
        'favorite_fruits', lstFruits.map((e) => e.toJson()).toList());
  }

  Future<List<Fruit>> loadFruits() async {
    final SharedPreferences prefs = await _prefs;
    return prefs.getStringList('favorite_fruits')?.map((e) {
          final Map<String, dynamic> fruit = jsonDecode(e);
          final Map<String, dynamic> nutritions =
              jsonDecode(fruit['nutritions']);
          fruit.remove('nutritions');
          fruit.addAll({'nutritions': nutritions});
          return Fruit.fromJson(fruit);
        }).toList() ??
        [];
  }

// BEERS -------------------------------------------------------------------------------------------
  Future<void> saveFavoriteBeers(List<Beer> lstBeer) async {
    final SharedPreferences prefs = await _prefs;
    prefs.setStringList(
        'favorite_beers', lstBeer.map((e) => jsonEncode(e.toJson())).toList());
  }

  Future<List<Beer>> loadBeers() async {
    final SharedPreferences prefs = await _prefs;
    return prefs.getStringList('favorite_beers')?.map((e) {
          final Map<String, dynamic> beer = jsonDecode(e);
          return Beer.fromJson(beer);
        }).toList() ??
        [];
  }
}
