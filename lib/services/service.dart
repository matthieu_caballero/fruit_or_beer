import 'package:fruit_or_beer/models/beer.dart';
import 'package:fruit_or_beer/models/fruit.dart';
import 'package:fruit_or_beer/services/beer_service.dart';
import 'package:fruit_or_beer/services/fruit_service.dart';
import 'package:fruit_or_beer/services/localstorage_service.dart';

class Service {
  final FruitService _fruitService;
  final BeerService _beerService;
  final LocalStorageService _localStorageService;

  Service(this._fruitService, this._beerService, this._localStorageService);

// FRUITS -------------------------------------------------------------------------------------------
  Future<List<Fruit>> loadFruits() async {
    return _fruitService.fetchAllFruits();
  }

  /// Ajoute le fruit aux favoris seulement si il n'est pas déjà présent
  Future<bool> addFavoriteFruit(Fruit fruit) async {
    final List<Fruit> lstFruits = await _localStorageService.loadFruits();
    if (!lstFruits.any((element) => element.id == fruit.id)) {
      _localStorageService.saveFavoriteFruits([...lstFruits, fruit]);
      return true;
    } else {
      return false;
    }
  }

  Future<List<Fruit>> getFavoriteFruits() async {
    return _localStorageService.loadFruits();
  }

// BEERS -------------------------------------------------------------------------------------------
  Future<Beer> loadRandomBeer() async {
    return _beerService.fetchRandomBeer();
  }

  Future<List<Beer>> loadAbvBeers(String abvGt, String abvLt) async {
    return _beerService.fetchAbvBeer(abvGt, abvLt);
  }

  Future<bool> addFavoriteBeer(Beer beer) async {
    final List<Beer> lstBeers = await _localStorageService.loadBeers();
    if (!lstBeers.any((element) => element.id == beer.id)) {
      _localStorageService.saveFavoriteBeers([...lstBeers, beer]);
      return true;
    } else {
      return false;
    }
  }

  Future<List<Beer>> getFavoriteBeers() async {
    return _localStorageService.loadBeers();
  }
}
