import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fruit_or_beer/models/fruit.dart';
import 'package:fruit_or_beer/services/service.dart';

class FruitStore extends Cubit<List<Fruit>> {
  final Service _service;
  FruitStore(this._service) : super([]);

  Future<void> loadFruits() async {
    final List<Fruit> lstFruits = await _service.loadFruits();
    emit(lstFruits);
  }

}
