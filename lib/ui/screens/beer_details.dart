import 'package:flutter/material.dart';
import 'package:fruit_or_beer/models/beer.dart';
import 'package:fruit_or_beer/services/service.dart';
import 'package:provider/provider.dart';

class BeerDetails extends StatelessWidget {
  const BeerDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Beer _beer = ModalRoute.of(context)?.settings.arguments as Beer;

    return Scaffold(
      appBar: AppBar(),
      body: Container(
        margin: const EdgeInsets.all(20.0),
        child: _beer != null
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    _beer.name,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 35),
                  ),
                  Text(
                    'Abv: ${_beer.abv}%',
                    style: const TextStyle(fontStyle: FontStyle.italic),
                  ),
                  Text(_beer.tagline),
                  Container(
                      margin: const EdgeInsets.symmetric(vertical: 10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Description',
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          Text(_beer.description)
                        ],
                      )),
                ],
              )
            : const Text('Sorry, no beer'),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          if (await Provider.of<Service>(context, listen: false)
              .addFavoriteBeer(_beer)) {
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text('Beer added to favorites')),
            );
          }
        },
        backgroundColor: Colors.green,
        child: const Icon(Icons.favorite),
      ),
    );
  }
}
