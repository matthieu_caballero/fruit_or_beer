import 'package:flutter/material.dart';
import 'package:fruit_or_beer/models/beer.dart';
import 'package:fruit_or_beer/services/service.dart';
import 'package:provider/provider.dart';

class BeerFavorites extends StatelessWidget {
  const BeerFavorites({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('My favorite beers'),
        ),
        body: FutureBuilder<List<Beer>>(
            initialData: const [],
            future:
                Provider.of<Service>(context, listen: false).getFavoriteBeers(),
            builder: (context, snapshot) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    snapshot.data!.isNotEmpty
                        ? Expanded(
                            child: ListView.builder(
                                itemCount: snapshot.data!.length,
                                itemBuilder: (context, index) {
                                  Beer beer = snapshot.data![index];
                                  return ListTile(
                                    title: Text(beer.name),
                                    subtitle: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text('Abv: ${beer.abv}'),
                                        Text(beer.tagline),
                                      ],
                                    ),
                                    onTap: () => Navigator.of(context)
                                        .pushNamed('/beerdetails',
                                            arguments: beer),
                                  );
                                }),
                          )
                        : Column(
                            children: [
                              const CircularProgressIndicator(
                                value: null,
                                strokeWidth: 8.0,
                                semanticsLabel: 'Linear progress indicator',
                              ),
                              Container(
                                margin:
                                    const EdgeInsets.symmetric(vertical: 12.0),
                                child:
                                    const Text('Loading or no favorite beers.'),
                              ),
                              const Text(
                                  'Try adding some beers to your favorites')
                            ],
                          )
                  ],
                ),
              );
            }));
  }
}
