import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fruit_or_beer/models/beer.dart';
import 'package:fruit_or_beer/services/service.dart';

class Beeropedia extends StatefulWidget {
  const Beeropedia({Key? key}) : super(key: key);

  @override
  State<Beeropedia> createState() => _BeeropediaState();
}

class _BeeropediaState extends State<Beeropedia> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  final TextEditingController _abvLessCtrl = TextEditingController();
  final TextEditingController _abvGreaterCtrl = TextEditingController();
  List<Beer> _lstBeer = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            Form(
                key: _formKey,
                child: Column(
                  children: [
                    TextFormField(
                      controller: _abvGreaterCtrl,
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ],
                      decoration: InputDecoration(
                        labelText: "ABV > ",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      validator: (String? value) {
                        if (value != null && value.isNotEmpty) {
                          return (int.parse(value) <
                                  int.parse(_abvLessCtrl.text))
                              ? null
                              : 'Doit être inférieur à ABV < (${_abvLessCtrl.text})';
                        } else {
                          return 'Le champ doit être renseigné';
                        }
                      },
                    ),
                    TextFormField(
                      controller: _abvLessCtrl,
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ],
                      decoration: InputDecoration(
                        labelText: "ABV < ",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      validator: (String? value) {
                        if (value != null && value.isNotEmpty) {
                          return (int.parse(value) >
                                  int.parse(_abvGreaterCtrl.text))
                              ? null
                              : 'Doit être supérieur à ABV > (${_abvGreaterCtrl.text})';
                        } else {
                          return 'Le champ doit être renseigné';
                        }
                      },
                    ),
                    ElevatedButton(
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            List<Beer> lstBeer = await context
                                .read<Service>()
                                .loadAbvBeers(
                                    _abvGreaterCtrl.text, _abvLessCtrl.text);
                            if (lstBeer.isNotEmpty) {
                              setState(() {
                                _lstBeer = lstBeer;
                              });
                            }
                          }
                        },
                        child: const Text('Valider'))
                  ],
                )),
            Expanded(
              child: ListView.builder(
                  itemCount: _lstBeer.length,
                  itemBuilder: (context, index) {
                    Beer beer = _lstBeer[index];
                    return ListTile(
                      title: Text(beer.name),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(beer.tagline),
                          Text('Abv : ${beer.abv}%')
                        ],
                      ),
                      onTap: () => Navigator.of(context)
                          .pushNamed('/beerdetails', arguments: beer),
                    );
                  }),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pushNamed('/beerfavorites');
        },
        backgroundColor: Colors.green,
        child: const Icon(Icons.favorite),
      ),
    );
  }
}
