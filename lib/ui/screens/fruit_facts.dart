import 'package:flutter/material.dart';
import 'package:fruit_or_beer/models/fruit.dart';
import 'package:fruit_or_beer/services/service.dart';
import 'package:provider/provider.dart';

class FruitFacts extends StatelessWidget {
  const FruitFacts({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Fruit _fruit = ModalRoute.of(context)?.settings.arguments as Fruit;

    return Scaffold(
      appBar: AppBar(),
      body: Container(
        margin: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              _fruit.name,
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 35),
            ),
            Text(
              _fruit.order + ' ' + _fruit.family,
              style: const TextStyle(fontStyle: FontStyle.italic),
            ),
            Text('Genus ' + _fruit.genus),
            Container(
                margin: const EdgeInsets.symmetric(vertical: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Nutritional Facts',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Text('Carbohydrates: ${_fruit.nutrition.carbohydrates}g'),
                    Text('Protein: ${_fruit.nutrition.protein}g'),
                    Text('Fat: ${_fruit.nutrition.fat}g'),
                    Text('Calories: ${_fruit.nutrition.calories}g'),
                    Text('Sugar: ${_fruit.nutrition.sugar}g'),
                  ],
                )),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          if (await Provider.of<Service>(context, listen: false)
              .addFavoriteFruit(_fruit)) {
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text('Fruit added to favorites')),
            );
          }
        },
        backgroundColor: Colors.green,
        child: const Icon(Icons.favorite),
      ),
    );
  }
}
