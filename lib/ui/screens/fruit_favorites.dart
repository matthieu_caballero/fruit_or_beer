import 'package:flutter/material.dart';
import 'package:fruit_or_beer/models/fruit.dart';
import 'package:fruit_or_beer/services/service.dart';
import 'package:provider/provider.dart';

class FruitFavorites extends StatelessWidget {
  const FruitFavorites({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('My favorite fruits'),
        ),
        body: FutureBuilder<List<Fruit>>(
            initialData: const [],
            future: Provider.of<Service>(context, listen: false)
                .getFavoriteFruits(),
            builder: (context, snapshot) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    snapshot.data!.isNotEmpty
                        ? Expanded(
                            child: ListView.builder(
                                itemCount: snapshot.data!.length,
                                itemBuilder: (context, index) {
                                  Fruit fruit = snapshot.data![index];
                                  return ListTile(
                                    title: Text(fruit.name),
                                    subtitle:
                                        Text(fruit.genus + ' ' + fruit.family),
                                    onTap: () => Navigator.of(context)
                                        .pushNamed('/fruitfacts',
                                            arguments: fruit),
                                  );
                                }),
                          )
                        : Column(
                            children: [
                              const CircularProgressIndicator(
                                value: null,
                                strokeWidth: 8.0,
                                semanticsLabel: 'Linear progress indicator',
                              ),
                              Container(
                                margin:
                                    const EdgeInsets.symmetric(vertical: 12.0),
                                child: const Text(
                                    'Loading or no favorite fruits.'),
                              ),
                              const Text(
                                  'Try adding some fruits to your favorites')
                            ],
                          )
                  ],
                ),
              );
            }));
  }
}
