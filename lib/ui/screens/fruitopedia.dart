import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fruit_or_beer/models/fruit.dart';
import 'package:fruit_or_beer/stores/fruit_store.dart';

class Fruitopedia extends StatelessWidget {
  const Fruitopedia({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 12.0),
        child: BlocBuilder<FruitStore, List<Fruit>>(builder: (context, state) {
          return ListView.builder(
              itemCount: state.length,
              itemBuilder: (context, index) {
                Fruit fruit = state[index];
                return ListTile(
                  title: Text(fruit.name),
                  subtitle: Text(fruit.genus + ' ' + fruit.family),
                  onTap: () => Navigator.of(context)
                      .pushNamed('/fruitfacts', arguments: fruit),
                );
              });
        }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pushNamed('/fruitfavorites');
        },
        backgroundColor: Colors.green,
        child: const Icon(Icons.favorite),
      ),
    );
  }
}
