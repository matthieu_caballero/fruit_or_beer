import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:fruit_or_beer/models/beer.dart';
import 'package:fruit_or_beer/models/fruit.dart';
import 'package:fruit_or_beer/services/service.dart';
import 'package:fruit_or_beer/stores/fruit_store.dart';
import 'package:provider/provider.dart';

class HeadsOrTails extends StatefulWidget {
  const HeadsOrTails({Key? key}) : super(key: key);

  @override
  State<HeadsOrTails> createState() => _HeadsOrTailsState();
}

class _HeadsOrTailsState extends State<HeadsOrTails> {
  late Map<String, dynamic> _rndObject;
  late bool _isLoading;

  @override
  void initState() {
    _isLoading = false;
    _rndObject = {
      'title': '?',
      'sentence': 'Tap the button to know what you\'re having next',
      'image': ''
    };

    super.initState();
  }

  void randomFruitOrBeer() async {
    loading();
    Random rnd = Random();
    Map<String, dynamic> rndObject = {};
    if (rnd.nextBool()) {
      final List<Fruit> lstFruit =
          Provider.of<FruitStore>(context, listen: false).state;
      final Fruit fruit = lstFruit[rnd.nextInt(lstFruit.length)];
      rndObject['sentence'] = 'It\'s a fruit!';
      rndObject['title'] = fruit.name;
      rndObject['image'] = 'images/Fruits.jpg';
    } else {
      final Beer beer =
          await Provider.of<Service>(context, listen: false).loadRandomBeer();
      rndObject['sentence'] = 'It\'s a beer!';
      rndObject['title'] = beer.name;
      rndObject['image'] = 'images/Beer.jpg';
    }
    setState(() {
      _rndObject = rndObject;
    });
    Timer(const Duration(seconds: 1), () => loading(false));
  }

  void loading([bool isLoading = true]) {
    setState(() {
      _isLoading = isLoading;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: _isLoading
            ? const CircularProgressIndicator(
                value: null,
                strokeWidth: 8.0,
                semanticsLabel: 'Linear progress indicator',
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 15.0),
                    child: Text(_rndObject['sentence'],
                        style: const TextStyle(fontSize: 25)),
                  ),
                  Text(_rndObject['title'],
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 35)),
                  if (_rndObject['image'].length > 0)
                    Expanded(
                      child: Image.asset(_rndObject['image']),
                    )
                ],
              ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: randomFruitOrBeer,
        backgroundColor: Colors.green,
        child: const Icon(Icons.shuffle),
      ),
    );
  }
}
