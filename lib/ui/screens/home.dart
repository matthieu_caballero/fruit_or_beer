import 'package:flutter/material.dart';
import 'package:fruit_or_beer/ui/screens/beeropedia.dart';
import 'package:fruit_or_beer/ui/screens/fruitopedia.dart';
import 'package:fruit_or_beer/ui/screens/heads_or_tails.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            bottom: const TabBar(tabs: [
              Tab(icon: Icon(Icons.shuffle)),
              Tab(icon: Icon(Icons.sports_golf)),
              Tab(icon: Icon(Icons.sports_bar))
            ]),
            title: const Center(child: Text('Fruit or Beer ?')),
          ),
          body: const TabBarView(
              children: [HeadsOrTails(), Fruitopedia(), Beeropedia()]),
        ));
  }
}
